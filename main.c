#include "declaracoes.h"


int main(int argc, char const *argv[])
{
	Teste();
	Menu_principal();
	return 0;
}



void Menu_principal()
{
    ITEM **my_items;
    int c;
    MENU *my_menu;
    int n_choices, i;
    // ITEM *cur_item;

    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    n_choices = ARRAY_SIZE(choices);
    my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

    for(i = 0; i < n_choices; ++i) {
        my_items[i] = new_item(choices[i], 0);
    }
    my_items[n_choices] = (ITEM *)NULL;

    my_menu = new_menu((ITEM **)my_items);
    mvprintw(LINES - 2, 0, "CTRL + C para sair");
    post_menu(my_menu);
    refresh();

    while((c = getch()) != KEY_F(1)) {
        switch(c) {
            case KEY_DOWN:
                menu_driver(my_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(my_menu, REQ_UP_ITEM);
                break;
            case 10: /* Enter */

                
                if(strcmp("Clientes",item_name(current_item(my_menu)))==0){
                	clear();
    							free_item(my_items[0]);
							    free_item(my_items[1]);
							    free_menu(my_menu);
                	endwin();

                	Clientes_menu();
                	break;
                }
                
                break;
        }
    }

    free_item(my_items[0]);
    free_item(my_items[1]);
    free_menu(my_menu);
    endwin();

}

void Clientes_menu()
{
    ITEM **my_items;
    int c;
    MENU *cliente_menu;
    int n_choices, i;
    // ITEM *cur_item;

    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    n_choices = ARRAY_SIZE(clientes_choices);
    my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

    for(i = 0; i < n_choices; ++i) {
        my_items[i] = new_item(clientes_choices[i], 0);
    }
    my_items[n_choices] = (ITEM *)NULL;

    cliente_menu = new_menu((ITEM **)my_items);
    mvprintw(LINES - 2, 0, "CTRL + C para sair");
    post_menu(cliente_menu);
    refresh();

    while((c = getch()) != KEY_F(1)) {
        switch(c) {
            case KEY_DOWN:
                menu_driver(cliente_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(cliente_menu, REQ_UP_ITEM);
                break;
            case 10: /* Enter */
                if(strcmp("Voltar",item_name(current_item(cliente_menu)))==0){
                	clear();
    							free_item(my_items[0]);
							    free_item(my_items[1]);
							    free_menu(cliente_menu);
                	endwin();;
                	Menu_principal();
                	break;
                }
                if(strcmp("Novo Cliente",item_name(current_item(cliente_menu)))==0){
                	clear();
    							free_item(my_items[0]);
							    free_item(my_items[1]);
							    free_menu(cliente_menu);
                	endwin();
                	novoCliente_form();
                	break;
                }
                if(strcmp("Todos Clientes",item_name(current_item(cliente_menu)))==0){
                	clear();
    							free_item(my_items[0]);
							    free_item(my_items[1]);
							    free_menu(cliente_menu);
                	endwin();
                	getTodosClientes();
                	break;
                }
                break;
        }
    }

    free_item(my_items[0]);
    free_item(my_items[1]);
    free_menu(cliente_menu);
    endwin();

}


void Medicamento_menu()
{
    ITEM **my_items;
    int c;
    MENU *my_menu;
    int n_choices, i;
    // ITEM *cur_item;

    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);

    n_choices = ARRAY_SIZE(Medicamentos_menu);
    my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

    for(i = 0; i < n_choices; ++i) {
        my_items[i] = new_item(Medicamentos_menu[i], 0);
    }
    my_items[n_choices] = (ITEM *)NULL;

    my_menu = new_menu((ITEM **)my_items);
    mvprintw(LINES - 2, 0, "CTRL + C para sair");
    post_menu(my_menu);
    refresh();

    while((c = getch()) != KEY_F(1)) {
        switch(c) {
            case KEY_DOWN:
                menu_driver(my_menu, REQ_DOWN_ITEM);
                break;
            case KEY_UP:
                menu_driver(my_menu, REQ_UP_ITEM);
                break;
            case 10: /* Enter */

                
                if(strcmp("Clientes",item_name(current_item(my_menu)))==0){
                	clear();
    							free_item(my_items[0]);
							    free_item(my_items[1]);
							    free_menu(my_menu);
                	endwin();

                	Clientes_menu();
                	break;
                }
                
                break;
        }
    }

    free_item(my_items[0]);
    free_item(my_items[1]);
    free_menu(my_menu);
    endwin();

}



void Teste(){
	pessoa novoCliente;

	strcpy(novoCliente.nome,"Jose Teste");			
	strcpy(novoCliente.CPF,"04425238195");
	strcpy(novoCliente.telefone,"984423213");
	strcpy(novoCliente.cidade,"Goiania");
	strcpy(novoCliente.estado,"GO");
	strcpy(novoCliente.descricao,"Não tem");
	salvaCliente(novoCliente);

	strcpy(novoCliente.nome,"Maria Teste");			
	strcpy(novoCliente.CPF,"04425248195");
	strcpy(novoCliente.telefone,"984423213");
	strcpy(novoCliente.cidade,"Goiania");
	strcpy(novoCliente.estado,"GO");
	strcpy(novoCliente.descricao,"Não tem");
	salvaCliente(novoCliente);

	strcpy(novoCliente.nome,"Mais um");			
	strcpy(novoCliente.CPF,"04425252195");
	strcpy(novoCliente.telefone,"984423213");
	strcpy(novoCliente.cidade,"Goiania");
	strcpy(novoCliente.estado,"GO");
	strcpy(novoCliente.descricao,"Não tem");
	salvaCliente(novoCliente);
}