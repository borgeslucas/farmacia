struct medicamento
{
	int id;
  char descricao[20];
  double codigo;
 	float valor_unitario_compra;
  float valor_unitario_venda;
  char data_validade[10];
  int total_estoque;
 
} typedef medicamento;


void salvaMedicamento(medicamento novoMedicamento){
	FILE * arquivo;
	arquivo = fopen(ARQUIVODEMEDICAMENTOS,"ab");
	if(arquivo == NULL){
		printf("ALGO ACONTECEU\n");
	}
	novoMedicamento.id=GetProximoId(1);
	SalvaIds(1, novoMedicamento.id);
	
	fwrite(&novoMedicamento, sizeof(struct medicamento),1,arquivo);
	fclose(arquivo);
}

void getTodosMedicamentos(){
	FILE * arquivo;
	medicamento Medicamento;
	arquivo = fopen(ARQUIVODEMEDICAMENTOS,"rb");
	int i=0;
	initscr();

	if(arquivo == NULL){
		printf("Arquivo não existe");
		return;
	}
   while(!feof(arquivo))
   {
    if(fread(&Medicamento, sizeof(struct medicamento), 1, arquivo) == 1)
    {
    	printf("%s\n", Medicamento.descricao);
    	mvprintw(i, 0, "%s",Medicamento.descricao);
   		i++;
    }
  }   
	fclose(arquivo);
}


void buscaMedicamento(int codigo){
	FILE * arquivo;
	medicamento Medicamento;
	arquivo = fopen(ARQUIVODEMEDICAMENTOS,"rb");
	if(arquivo == NULL){
		printf("Não existe medicamentos cadastrados\n");
		return;
	}
   while(!feof(arquivo))
   {
    if(fread(&Medicamento, sizeof(struct medicamento), 1, arquivo) == 1)
    {
    	if(Medicamento.codigo == codigo){
	    	printf("%d\n", Medicamento.codigo);
	    	printf("%s\n", Medicamento.descricao);
    	
    	} else {
    	
    		printf("Medicamento não existe\n");
    	
    	}
    }
  }   
	fclose(arquivo);
}



