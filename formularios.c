
void Clientes_menu();
void novoCliente_form()
{	
	int fields_number=7;
	FIELD *field[fields_number];
	FORM  *my_form;
	int ch;
	int i;
	/* Initialize curses */
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

	/* Initialize the fields */
	field[0] = new_field(1, 25, 2, 10, 0, 0);
	field[1] = new_field(1, 11, 5, 10, 0, 0);
	field[2] = new_field(1, 11, 8, 14, 4, 0);
	field[3] = new_field(1, 2, 11, 12, 0, 0);
	field[4] = new_field(1, 10, 14, 12, 0, 0);
	field[5] = new_field(1, 15, 17, 16, 0, 0);
	

	field[6] = NULL;

	for (int i = 0; i < 7; ++i)
	{
		set_field_back(field[i], A_UNDERLINE); 

		field_opts_on(field[i], O_AUTOSKIP); 
		
	
	}

	set_field_type(field[1], TYPE_NUMERIC, 1, 11,11);
	set_field_type(field[2], TYPE_NUMERIC, 1, 10,10);

	/* Create the form and post it */
	my_form = new_form(field);
	post_form(my_form);
	refresh();
	
	mvprintw(2, 4, "Nome:");
	mvprintw(3, 4, "Somente Letras");
	
	mvprintw(5, 4, "CPF:");
	mvprintw(6, 4, "11 digitos, somente numeros");

	mvprintw(8, 4, "Telefone:");
	mvprintw(9, 4, "10 digitos, somente numeros");

	mvprintw(11, 4, "Estado:");
	mvprintw(12, 4, "Duas letras: Exemplo DF");

	mvprintw(14, 4, "Cidade:");
	mvprintw(15, 4, "Somente letras");

	mvprintw(17, 4, "Descricao:");
	mvprintw(18, 4, "Texto livre");

	mvprintw(20, 4, "Aperte enter para terminar");
	refresh();

	/* Loop through to get user requests */
	while((ch = getch()) != KEY_F(1))
	{	switch(ch)
		{	case KEY_DOWN:
				/* Go to next field */
				form_driver(my_form, REQ_NEXT_FIELD);
				/* Go to the end of the present buffer */
				/* Leaves nicely at the last character */
				form_driver(my_form, REQ_END_LINE);
				break;
			case KEY_UP:
				/* Go to previous field */
				form_driver(my_form, REQ_PREV_FIELD);
				form_driver(my_form, REQ_END_LINE);
				break;
			
			case 10:

				endwin();
				pessoa novoCliente;				
				int i;
				for(i=0;i<5;i++){
					if(field_buffer(field[0],0)==NULL)
					{
						mvprintw(21, 4, "Há um campo nulo");
						break;
					}
				}
				strcpy(novoCliente.nome,field_buffer(field[0],0));
				
				printf("%s\n", novoCliente.nome);

				strcpy(novoCliente.CPF,field_buffer(field[1],0));
				printf("%s\n", novoCliente.CPF);
				strcpy(novoCliente.telefone,field_buffer(field[2],0));
				printf("%s\n", novoCliente.telefone);
				strcpy(novoCliente.cidade,field_buffer(field[3],0));
				printf("%s\n", novoCliente.cidade);
				strcpy(novoCliente.estado,field_buffer(field[4],0));
				printf("%s\n", novoCliente.estado);
				strcpy(novoCliente.descricao,field_buffer(field[5],0));
				salvaCliente(novoCliente);
				endwin();
				clear();
				Clientes_menu();
				
				
				break;


				
			case KEY_BACKSPACE:
			case 127:
				form_driver(my_form, REQ_DEL_PREV);
				break;
			case KEY_DC:
				form_driver(my_form, REQ_DEL_CHAR);
				break;

			default:
				/* If this is a normal character, it gets */
				/* Printed				  */	
				form_driver(my_form, ch);
				break;
		}
	}

	/* Un post form and free the memory */
	unpost_form(my_form);
	free_form(my_form);
	for (i = 0; i < fields_number; i++)
	{
		free_field(field[i]);
	}
	
	endwin();
}
