#define ARQUIVODEID "bin/ids.bin"

struct controleID{
	int IdCliente;
	int IdProduto;
	int IdVenda;
} typedef control;

void ChecaArquivoIds(){
	control controles;

	FILE * arquivoID;
	
	arquivoID = fopen(ARQUIVODEID,"rb");
	
	if(arquivoID == NULL){
		printf("Criando Arquivo com os ids\n");
		arquivoID = fopen(ARQUIVODEID,"wb");
		controles.IdProduto=0;
		controles.IdVenda=0;
		controles.IdCliente=0;
		fwrite(&controles, sizeof(struct controleID),sizeof(controles),arquivoID);
		fclose(arquivoID);
	}
	fclose(arquivoID);
} 


void SalvaIds(int tipo, int novo){
	control controles;

	FILE * arquivoID;

	ChecaArquivoIds();

	arquivoID = fopen(ARQUIVODEID,"rb");

	fread(&controles, sizeof(struct controleID), 1, arquivoID);

	if(tipo == 1){
		controles.IdProduto = novo;
	}

	if(tipo == 2){
		controles.IdCliente = novo;
	}

	if(tipo == 3){
		controles.IdVenda   = novo;
	}

	//fclose(arquivoID);	


	arquivoID = fopen(ARQUIVODEID,"wb");
	
	fwrite(&controles, sizeof(struct controleID),sizeof(controles),arquivoID);
	fwrite(&controles, sizeof(struct controleID),sizeof(controles),arquivoID);
	
	fclose(arquivoID);
}	

int GetProximoId(int tipo){
	FILE * arquivoID;
	control controles;	
	int result;
	arquivoID = fopen(ARQUIVODEID,"rb");
	if(arquivoID == NULL){
		SalvaIds(1,0);
		SalvaIds(2,0);
		SalvaIds(3,0);
	} else{
								
			if(fread (&controles, sizeof(struct controleID), 1, arquivoID)==0){
				printf("ERRO ARQUIVO VAZIO\n");
				return 404;
			}
		  if(tipo == 1){
		  	return (int) controles.IdProduto+1;
		  }
		  if(tipo == 2){

		  	return (int) controles.IdCliente+1;
		  }
			if(tipo == 3){
				
				return (int) controles.IdVenda+1;
			}
		
	}
	fclose(arquivoID);
}		